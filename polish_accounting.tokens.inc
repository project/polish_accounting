<?php

/**
 * @file
 * Token logic.
 */

declare(strict_types=1);

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function polish_accounting_token_info_alter(&$data) {

  $data['tokens']['date']['accounting_document'] = [
    'name' => t("Document sale date"),
    'description' => t("The sale date of the docyment."),
    'type' => 'date',
  ];
}

function polish_accounting_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  if ($type !== 'date') {
    return;
  }
  if (!\array_key_exists('accounting_document', $tokens)) {
    return;
  }
  $token_service = \Drupal::token();
}
