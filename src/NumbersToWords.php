<?php

declare(strict_types=1);

namespace Drupal\polish_accounting;

/**
 * Utility class that converts numbers to words.
 */
final class NumbersToWords {
  private const DICTIONARIES = [
    'pl' => [
      0 => 'zero',
      1 => 'jeden',
      2 => 'dwa',
      3 => 'trzy',
      4 => 'cztery',
      5 => 'pięć',
      6 => 'sześć',
      7 => 'siedem',
      8 => 'osiem',
      9 => 'dziewięć',
      10 => 'dziesięć',
      11 => 'jedenaście',
      12 => 'dwanaście',
      13 => 'trzynaście',
      14 => 'czternaście',
      15 => 'piętnaście',
      16 => 'szesnaście',
      17 => 'siedemnaście',
      18 => 'osiemnaście',
      19 => 'dziewiętnaście',
      20 => 'dwadzieścia',
      30 => 'trzydzieści',
      40 => 'czterdzieści',
      50 => 'pięćdziesiąt',
      60 => 'sześćdziesiąt',
      70 => 'siedemdziesiąt',
      80 => 'osiemdziesiąt',
      90 => 'dziewięćdziesiąt',
      100 => 'sto',
      200 => 'dwieście',
      300 => 'trzysta',
      400 => 'czterysta',
      500 => 'pięćset',
      600 => 'sześćset',
      700 => 'siedemset',
      800 => 'osiemset',
      900 => 'dziewięćset',
      1000 => [
        'singular' => 'tysiąc',
        'transitional' => 'tysiące',
        'plural' => 'tysięcy',
      ],
      1000000 => [
        'singular' => 'milion',
        'transitional' => 'miliony',
        'plural' => 'milionów',
      ],
      1000000000 => [
        'singular' => 'miliard',
        'transitional' => 'miliardy',
        'plural' => 'miliardów',
      ],
      1000000000000 => [
        'singular' => 'bilion',
        'transitional' => 'biliardy',
        'plural' => 'biliardów',
      ],
      1000000000000000 => [
        'singular' => 'trylion',
        'transitional' => 'tryliony',
        'plural' => 'trylionów',
      ],
      1000000000000000000 => [
        'singular' => 'tryliard',
        'transitional' => 'tryliardy',
        'plural' => 'tryliardów',
      ],
      'comma' => 'przecinek',
      'minus' => 'minus',
    ],
    'en' => [
      0 => 'zero',
      1 => 'one',
      2 => 'two',
      3 => 'three',
      4 => 'four',
      5 => 'five',
      6 => 'six',
      7 => 'seven',
      8 => 'eight',
      9 => 'nine',
      10 => 'ten',
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirteen',
      14 => 'fourteen',
      15 => 'fifteen',
      16 => 'sixteen',
      17 => 'seventeen',
      18 => 'eighteen',
      19 => 'nineteen',
      20 => 'twenty',
      30 => 'thirty',
      40 => 'fourty',
      50 => 'fifty',
      60 => 'sixty',
      70 => 'seventy',
      80 => 'eighty',
      90 => 'ninety',
      100 => 'one hundred',
      200 => 'two hundred',
      300 => 'three hundred',
      400 => 'four hundred',
      500 => 'five hundred',
      600 => 'six hundred',
      700 => 'seven hundred',
      800 => 'eight hundred',
      900 => 'nine hundred',
      1000 => [
        'singular' => 'thousand',
        'transitional' => 'thousand',
        'plural' => 'thousand',
      ],
      1000000 => [
        'singular' => 'million',
        'transitional' => 'million',
        'plural' => 'million',
      ],
      1000000000 => [
        'singular' => 'billion',
        'transitional' => 'billion',
        'plural' => 'billion',
      ],
      1000000000000 => [
        'singular' => 'trillion',
        'transitional' => 'trillion',
        'plural' => 'trillion',
      ],
      1000000000000000 => [
        'singular' => 'quadrillion',
        'transitional' => 'quadrillion',
        'plural' => 'quadrillion',
      ],
      1000000000000000000 => [
        'singular' => 'quintillion',
        'transitional' => 'quintillion',
        'plural' => 'quintillion',
      ],
      'comma' => 'comma',
      'minus' => 'minus',
    ],
  ];

  private const SEPARATOR = ' ';

  /**
   * The converting method.
   */
  public static function toWords(float $number, ?int $decimals = NULL, string $lang = 'en') {
    if (!\array_key_exists($lang, self::DICTIONARIES)) {
      throw InvalidArgumentException(sprintf('Unsupported language in number to words conversion: %s', $lang));
    }

    if ($number > 1000000000000000000 || $number < -1000000000000000000) {
      throw InvalidArgumentException(sprintf('%s? Are you kidding me?', $number));
    }

    if ($decimals !== NULL && $decimals > 6) {
      throw InvalidArgumentException(sprintf('Max 6 decimals supported.'));
    }

    if ($decimals !== NULL) {
      $number = \round($number, $decimals);
    }

    $dictionary = self::DICTIONARIES[$lang];

    if ($number < 0) {
      return $dictionary['minus'] . self::SEPARATOR . self::toWords(\abs($number), NULL, $lang);
    }

    $whole = \floor($number);

    // If not rounding, float precision comes into play and spoils the result.
    $fraction = \round($number - $whole, 6);

    if ($whole < 21) {
      $string = $dictionary[$whole];
    }
    elseif ($whole < 100) {
      $tens = ((int) ($whole / 10)) * 10;
      $units = $whole % 10;
      $string = $dictionary[$tens];
      if ($units !== 0) {
        $string .= self::SEPARATOR . $dictionary[$units];
      }
    }
    elseif ($whole < 1000) {
      $remainder = $whole % 100;
      $hundreds = $whole - $remainder;
      $string = $dictionary[$hundreds] . self::SEPARATOR;
      if ($remainder) {
        $string .= self::toWords($remainder, NULL, $lang);
      }
    }
    else {
      $baseUnit = \pow(1000, \floor(\log($whole, 1000)));
      $numBaseUnits = (int) ($whole / $baseUnit);
      $remainder = $whole % $baseUnit;
      if ($numBaseUnits === 1) {
        $string = $dictionary[$baseUnit]['singular'];
      }
      elseif ($numBaseUnits > 1 && $numBaseUnits < 5) {
        $string = self::toWords($numBaseUnits, NULL, $lang) . self::SEPARATOR . $dictionary[$baseUnit]['transitional'];
      }
      else {
        $string = self::toWords($numBaseUnits, NULL, $lang) . self::SEPARATOR . $dictionary[$baseUnit]['plural'];
      }
      if ($remainder) {
        $string .= self::SEPARATOR . self::toWords($remainder, NULL, $lang);
      }
    }

    if ($fraction > 0) {
      $fraction = substr((string) $fraction, 2);
      $string .= self::SEPARATOR . $dictionary['comma'] . self::SEPARATOR;
      $words = [];
      foreach (\str_split($fraction) as $number) {
        $words[] = $dictionary[$number];
      }
      $string .= \implode(self::SEPARATOR, $words);
    }

    return $string;
  }

}
