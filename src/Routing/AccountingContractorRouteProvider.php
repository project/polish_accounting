<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Routing;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;

final class AccountingContractorRouteProvider extends DefaultHtmlRouteProvider {

}
