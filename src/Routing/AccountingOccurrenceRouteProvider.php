<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

final class AccountingOccurrenceRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $label = $entity_type->getCollectionLabel();

    $route = new Route($entity_type->getLinkTemplate('collection'));
    $route
      ->addDefaults([
        '_entity_list' => $entity_type->id(),
        '_title' => $label->getUntranslatedString(),
        '_title_arguments' => $label->getArguments(),
        '_title_context' => $label->getOption('context'),
      ])
      ->setRequirement('_permission', 'view accounting');

    return $route;
  }

}
