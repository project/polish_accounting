<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\ListBuilder;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of document entities.
 */
class AccountingDocumentListBuilder extends AccountingEntityListBuilderBase {

  protected const DATE_FIELD = 'sale_date';

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $row = [
      'document_no' => $this->t('Number'),
      'sale_date' => $this->t('Sale date'),
      'buyer' => $this->t('Buyer'),
      'amount' => $this->t('Amount'),
      'file' => $this->t('File'),
    ];
    $row['operations'] = $this->t('Operations');
    return $row + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['document_no']['data'] = $entity->toLink();
    $row['sale_date']['data'] = $entity->sale_date->view([
      'type' => 'datetime_default',
      'label' => 'hidden',
      'settings' => [
        'format_type' => 'short',
      ],
    ]);
    $row['buyer']['data'] = $entity->buyer->view([
      'type' => 'entity_reference_label',
      'label' => 'hidden',
      'settings' => [
        'link' => TRUE,
      ],
    ]);
    $row['amount']['data'] = $entity->items->view([
      'type' => 'document_items_amount',
      'label' => 'hidden',
    ]);
    $row['file']['data'] = $entity->file->view([
      'type' => 'file_default',
      'label' => 'hidden',
    ]);

    return $row + parent::buildRow($entity);
  }

}
