<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\ListBuilder;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of occurrence entities.
 */
class AccountingOccurrenceListBuilder extends AccountingEntityListBuilderBase {

  protected const DATE_FIELD = 'date';

  /**
   * Totals for the last row.
   */
  private array $totals = [
    'income' => 0,
    'cost' => 0,
    'vat' => 0,
  ];

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $row = [
      'id' => $this->t('Document ID'),
      'document_no' => $this->t('Number'),
      'date' => $this->t('Occurrence date'),
      'buyer' => $this->t('Buyer'),
      'income' => $this->t('Income'),
      'cost' => $this->t('Cost'),
      'vat' => $this->t('VAT'),
      'file' => $this->t('File'),
    ];
    $row['operations'] = $this->t('Operations');
    return $row + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['id']['data'] = $entity->id();
    $row['document_no']['data'] = $entity->toLink();
    $row['date']['data'] = $entity->date->view([
      'type' => 'datetime_default',
      'label' => 'hidden',
      'settings' => [
        'format_type' => 'short',
      ],
    ]);
    $row['buyer']['data'] = $entity->contractor->view([
      'type' => 'entity_reference_label',
      'label' => 'hidden',
      'settings' => [
        'link' => TRUE,
      ],
    ]);
    $row['income']['data'] = $entity->getIncome();
    $this->totals['income'] += $row['income']['data'];
    $row['cost']['data'] = $entity->getCost();
    $this->totals['cost'] += $row['cost']['data'];
    $row['vat']['data'] = $entity->getVat();

    if ($row['income']['data'] > $row['cost']['data']) {
      $this->totals['vat'] += $row['vat']['data'];
    }
    else {
      $this->totals['vat'] -= $row['vat']['data'];
    }

    $file_field = NULL;
    if (!$entity->file->isEmpty()) {
      $file_field = $entity->file;
    }
    elseif (!$entity->document->isEmpty() && !$entity->document->entity->file->isEmpty()) {
      $file_field = $entity->document->entity->file;
    }
    if ($file_field !== NULL) {
      $row['file']['data'] = $file_field->view([
        'type' => 'file_default',
        'label' => 'hidden',
      ]);
    }
    else {
      $row['file']['data'] = '';
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    // Add Totals row.
    $build['table']['#rows'][] = [
      'data' => [
        'id' => [
          'data' => $this->t('Totals'),
          'colspan' => 4,
        ],
        'income' => $this->totals['income'],
        'cost' => $this->totals['cost'],
        'vat' => $this->totals['vat'],
        'reminder' => [
          'data' => '',
          'colspan' => 2,
        ],
      ],
      'class' => ['accounting-totals'],
    ];

    return $build;
  }

}
