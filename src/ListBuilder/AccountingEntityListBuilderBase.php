<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\ListBuilder;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Url;
use Drupal\polish_accounting\AccountingDatesTrait;

/**
 * Base class for accounting entity lists.
 */
abstract class AccountingEntityListBuilderBase extends EntityListBuilder {
  use AccountingDatesTrait;

  protected const DATE_FIELD = 'date';

  /**
   * The current request.
   */
  private string $monthFilterValue;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * The obvious.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    Request $request,
  ) {
    parent::__construct($entity_type, $storage);

    $this->monthFilterValue = $request->query->get('month', '');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    if ($this->monthFilterValue === '') {
      return $this->getMonthEntityIdsFromStr('', static::DATE_FIELD, $this->limit);
    }

    $date_str = $this->monthFilterValue . '-01';
    return $this->getMonthEntityIdsFromStr($date_str, static::DATE_FIELD, $this->limit);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];

    $build['date_links'] = [
      '#theme' => 'links',
      '#links' => [],
      '#attributes' => ['class' => ['accounting-date-links']],
    ];
    $url = Url::fromRoute('entity.' . $this->entityTypeId . '.collection');

    foreach ($this->getFilterDates($this->monthFilterValue) as $filter_value) {
      $query = ['month' => $filter_value];
      $url_clone = clone $url;
      $url_clone->setOption('query', $query);
      $build['date_links']['#links'][] = [
        'title' => $filter_value,
        'url' => $url_clone,
      ];
    }

    $build += parent::render();

    $build['#attached']['library'][] = 'polish_accounting/ui';
    $build['#cache']['contexts'] = ['url.query_args'];

    return $build;
  }

}
