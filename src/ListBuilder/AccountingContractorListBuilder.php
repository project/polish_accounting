<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of contractor entities.
 */
class AccountingContractorListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $row = [
      'name' => $this->t('Name'),
      'tax_id' => $this->t('Tax ID'),
    ];
    $row['operations'] = $this->t('Operations');
    return $row + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['name']['data'] = $entity->label();
    $row['tax_id']['data'] = $entity->tin->value;

    return $row + parent::buildRow($entity);
  }

}
