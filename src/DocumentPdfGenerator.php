<?php

declare(strict_types=1);

namespace Drupal\polish_accounting;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\polish_accounting\Entity\AccountingDocument;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Document PDF generator service class.
 */
class DocumentPdfGenerator {

  private const TEMPLATES_DIR = 'templates/document_pdf';

  /**
   * Extension path resolver.
   */
  private ExtensionPathResolver $extensionPathResolver;

  /**
   * File system service.
   */
  private FileSystemInterface $fileSystem;

  /**
   * File repository service.
   */
  private FileRepositoryInterface $fileRepository;

  /**
   * The file URL generator.
   */
  private FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * Twig service.
   */
  private TwigEnvironment $twig;

  /**
   * The obvious.
   */
  public function __construct(
    ExtensionPathResolver $extension_path_resolver,
    FileSystemInterface $file_system,
    FileRepositoryInterface $file_repository,
    FileUrlGeneratorInterface $file_url_generator,
    TwigEnvironment $twig
  ) {
    $this->extensionPathResolver = $extension_path_resolver;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->fileUrlGenerator = $file_url_generator;
    $this->twig = $twig;
  }

  /**
   * Get available template files.
   *
   * @todo Cache result.
   */
  public function getTemplates(): array {
    $locations = [
      // @todo Add additional locations or a hook/event.
      'core' => $this->extensionPathResolver->getPath('module', 'polish_accounting') . '/' . self::TEMPLATES_DIR,
    ];

    $templates = [];
    foreach ($locations as $location_name => $location) {
      foreach ($this->fileSystem->scanDirectory($location, '/.html.twig/') as $file) {
        $name = \substr($file->name, 0, -5);
        $language = \substr($name, 0, \strpos($name, '_'));
        $id = $location_name . '_' . $name;
        $templates[$id] = [
          'name' => "$name ($location_name)",
          'uri' => $file->uri,
          'language' => $language,
        ];
      }
    }

    return $templates;
  }

  /**
   * Generate PDF from a accounting document entity.
   */
  public function generatePdf(AccountingDocument $document, string $template) {
    $issue_date = $document->issue_date->value;
    $destination_dir = 'private://accounting/' . \substr($issue_date, 0, \strrpos($issue_date, '-'));
    if (!$this->fileSystem->prepareDirectory($destination_dir, FileSystemInterface::CREATE_DIRECTORY)) {
      throw new \Exception(\sprintf('Unable to save document file, directory %s not writeable.', $destination_dir), 8);
    }

    $templates = $this->getTemplates();
    if (!\array_key_exists($template, $templates)) {
      throw new \InvalidArgumentException(sprintf('Template %s doesn\'t exist.', $template));
    }
    $template_data = $templates[$template];

    $variables = $this->preparePdfVariables($document, $template_data['language']);
    $html = $this->twig
      ->load($template_data['uri'])
      ->render($variables);

    $options = new Options();
    $options->set('defaultFont', 'DejaVu Sans');
    $options->set('isRemoteEnabled', TRUE);
    $options->set('isHtml5ParserEnabled', TRUE);
    $options->set('isPhpEnabled', TRUE);
    $dompdf = new Dompdf($options);
    $dompdf->setPaper('A4');
    $dompdf->loadHtml($html);
    $dompdf->render();

    // We have to render twice just to get page count..
    // @todo track https://github.com/dompdf/dompdf/issues/1636
    $html = \str_replace('<span id="total-pages"></span>', '<span id="total-pages">' . $dompdf->getCanvas()->get_page_count() . '</span>', $html);
    $dompdf = new Dompdf($options);
    $dompdf->setPaper('A4');
    $dompdf->loadHtml($html);
    $dompdf->render();

    $file_uri = $destination_dir . '/' . $document->document_no->value . '.pdf';
    return $this->fileRepository->writeData($dompdf->output(), $file_uri, FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * Prepares variables for document twig template.
   */
  private function preparePdfVariables(AccountingDocument $document, string $language): array {
    $contractors = [
      'seller' => $document->get('seller')->entity,
      'buyer' => $document->get('buyer')->entity,
    ];

    $document_items = $document->get('items');
    $logo_url = NULL;
    if (!$document->seller->entity->logo->isEmpty()) {
      $logo_uri = $document->seller->entity->logo->entity->uri->value;
      $logo_url = $this->fileUrlGenerator->generate($logo_uri);
      $logo_url->setAbsolute();
    }

    $variables = [
      'logo_url' => $logo_url === NULL ? '' : $logo_url->toString(),
      'document_no' => $document->get('document_no')->value,
      'totals' => $document_items->getTotals(),
      'buyer' => [],
      'seller' => [],
      'items' => [],
    ];
    $variables['totals']['words'] = NumbersToWords::toWords($variables['totals']['gross'], 2, $language);

    // Dates: convert to DateTime objects for twig formatting.
    $date_fields = ['issue_date', 'due_date'];
    if ($document->get('sale_date')->value !== $document->get('issue_date')->value) {
      $date_fields[] = 'sale_date';
    }
    foreach ($date_fields as $date_field) {
      $variables[$date_field] = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $document->get($date_field)->value);
    }

    // Contractors.
    foreach (['buyer', 'seller'] as $contractor_type) {
      foreach ([
        'name',
        'address',
        'postal_code',
        'city',
        'country',
        'tin',
        'account_no',
        'bic',
      ] as $field) {
        $variables[$contractor_type][$field] = $contractors[$contractor_type]->get($field)->value;
      }
    }

    // Document items.
    foreach ($document_items as $delta => $document_item) {
      $template_item = ['no' => $delta + 1];
      foreach ([
        'name',
        'net_price',
        'unit',
        'quantity',
        'net_total',
        'vat',
        'vat_amount',
        'gross_total',
      ] as $property_name) {
        // @todo Fix AccountingDocumentItem __get and get methods.
        $property = $document_item->get($property_name);
        if (\is_object($property)) {
          $template_item[$property_name] = $property->getValue();
        }
        else {
          $template_item[$property_name] = $property;
        }
      }
      $variables['items'][] = $template_item;
    }

    return $variables;
  }

}
