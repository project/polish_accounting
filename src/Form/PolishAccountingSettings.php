<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\polish_accounting\TaxMethodManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Global accounting settings form.
 */
final class PolishAccountingSettings extends ConfigFormBase {

  private const CONFIG_NAME = 'polish_accounting.settings';

  protected EntityTypeManagerInterface $entityTypeManager;
  protected TaxMethodManager $taxMethodManager;

  /**
   * The constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    TaxMethodManager $tax_method_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->taxMethodManager = $tax_method_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.accounting_tax_method')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'polish_accounting_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $form['info'] = [
      '#markup' => $this->t('For the module to work properly, private file system needs to be configured on the site.'),
    ];

    $form['default_seller'] = [
      '#title' => $this->t('Default seller'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'accounting_contractor',
    ];
    $default_seller = $config->get('default_seller');
    if ($default_seller !== NULL && $default_seller !== '') {
      $contractor_entities = $this->entityTypeManager->getStorage('accounting_contractor')->loadByProperties(['uuid' => $default_seller]);
      if (\count($contractor_entities) !== 0) {
        $form['default_seller']['#default_value'] = reset($contractor_entities);
      }
      else {
        $config->set('default_seller', '');
        $config->save();
      }
    }

    $form['payment_deadline_days'] = [
      '#title' => $this->t('Default payment deadline in days'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
      '#default_value' => $config->get('payment_deadline_days'),
    ];

    // Tax plugin selection and configuration.
    $wrapper_id = 'tax-plugin-configuration-wrapper';
    $plugin_options = [
      '' => $this->t('-- Select tax plugin --'),
    ];
    foreach ($this->taxMethodManager->getDefinitions() as $definition) {
      $plugin_options[$definition['id']] = $definition['title'];
    }
    $form['tax_plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Tax method plugin'),
      '#options' => $plugin_options,
      '#default_value' => $config->get('tax_plugin_id'),
      '#ajax' => [
        'callback' => [$this, 'pluginSettingsAjax'],
        'wrapper' => $wrapper_id,
      ],
    ];

    $form['tax_plugin_settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => ['id' => $wrapper_id],
    ];

    $plugin_id = $form_state->getValue('tax_plugin_id');
    $plugin = NULL;
    if ($plugin_id === NULL) {
      $plugin_id = $config->get('tax_plugin_id');
      if ($plugin_id !== NULL && $plugin_id !== '') {
        $plugin = $this->taxMethodManager->createInstance($plugin_id, $config->get('tax_plugin_settings'));
      }
    }
    elseif ($plugin_id !== '') {
      $plugin = $this->taxMethodManager->createInstance($plugin_id);
    }

    if ($plugin !== NULL) {
      $form['tax_plugin_settings']['#type'] = 'details';
      $form['tax_plugin_settings']['#open'] = TRUE;
      $form['tax_plugin_settings']['#title'] = $this->t('@plugin settings', [
        '@plugin' => $plugin->getPluginDefinition()['title'],
      ]);
      $form['tax_plugin_settings'] = $plugin->buildConfigurationForm($form['tax_plugin_settings'], $form_state);
    }

    $form['default_rate'] = [
      '#title' => $this->t('Default unit net price'),
      '#description' => $this->t('Default unit net price for invoice item'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#default_value' => $config->get('default_rate'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Plugin settings AJAX callback.
   */
  public function pluginSettingsAjax(array $form, FormStateInterface $form_state) {
    return $form['tax_plugin_settings'];
  }

  /**
   * Helper function that processes config values.
   */
  private function processValues(array &$values): void {
    $contractor_entity = $this->entityTypeManager->getStorage('accounting_contractor')->load($values['default_seller']);
    if ($contractor_entity !== NULL) {
      $values['default_seller'] = $contractor_entity->uuid();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $form_state->cleanValues();
    $values = $form_state->getValues();
    $this->processValues($values);

    foreach ($values as $name => $value) {
      $config->set($name, $value);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
