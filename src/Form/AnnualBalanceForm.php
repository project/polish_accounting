<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\polish_accounting\TaxMethodManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the annual balance form.
 */
final class AnnualBalanceForm extends FormBase {

  private ?array $balanceEntities = NULL;

  protected EntityTypeManagerInterface $entityTypeManager;
  protected ImmutableConfig $config;
  protected TaxMethodManager $taxMethodManager;

  private bool $readOnly = TRUE;

  /**
   * The obvious.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    TaxMethodManager $tax_method_manager,
    AccountProxyInterface $current_user
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('polish_accounting.settings');
    $this->taxMethodManager = $tax_method_manager;

    $user = $entity_type_manager->getStorage('user')->load($current_user->id());
    if ($user->hasPermission('administer accounting') || $user->hasPermission('use accounting')) {
      $this->readOnly = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.accounting_tax_method'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'accounting_annual_balance';
  }

  /**
   * Get month balance entities.
   */
  private function getBalanceEntities(int $year): array {
    if ($this->balanceEntities !== NULL) {
      return $this->balanceEntities;
    }

    $storage = $this->entityTypeManager->getStorage('accounting_month');
    $entities = $storage->loadByProperties(['year' => $year]);
    $this->balanceEntities = [];
    foreach ($entities as $entity) {
      $this->balanceEntities[$entity->month->value] = $entity;
    }
    for ($i = 1; $i <= 12; $i++) {
      if (!\array_key_exists($i, $this->balanceEntities)) {
        $this->balanceEntities[$i] = $storage->create([
          'month' => $i,
          'year' => $year,
        ]);
      }
    }

    ksort($this->balanceEntities, SORT_NUMERIC);

    return $this->balanceEntities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $year = NULL) {
    $year = $this->getYear($form_state);
    $this->getBalanceEntities($year);

    // Tax table.
    $form['table_tax'] = [
      '#type' => 'table',
      '#header' => [
        'month' => $this->t('Month'),
        'income' => $this->t('Income'),
        'cost' => $this->t('Costs'),
        'revenue' => $this->t('Revenue'),
        'income_tax_paid' => $this->t('Paid tax'),
        'vat' => $this->t('VAT sum'),
        'vat_paid' => $this->t('Paid VAT'),
        'notes' => $this->t('Notes'),
      ],
    ];

    $totals = [
      'income' => 0,
      'cost' => 0,
      'revenue' => 0,
      'vat' => 0,
    ];
    foreach ($this->balanceEntities as $month => $month_data) {
      $form['table_tax'][$month] = [
        'month' => ['#markup' => $month],
        'income' => ['#markup' => $month_data->income->value],
        'cost' => ['#markup' => $month_data->cost->value],
        'revenue' => [
          '#markup' => $month_data->income->value - $month_data->cost->value,
        ],
        'income_tax_paid' => [
          '#type' => 'textfield',
          '#default_value' => $month_data->income_tax_paid->value,
        ],
        'vat' => ['#markup' => $month_data->vat->value],
        'vat_paid' => [
          '#type' => 'textfield',
          '#default_value' => $month_data->vat_paid->value,
        ],
        'notes' => [
          '#type' => 'textfield',
          '#default_value' => $month_data->notes->value,
        ],
      ];

      foreach (\array_keys($totals) as $key) {
        $totals[$key] += $form['table_tax'][$month][$key]['#markup'];
      }
    }

    $this->addTotalsRow($form['table_tax'], [
      'income',
      'cost',
      'revenue',
      'income_tax_paid',
      'vat',
      'vat_paid',
    ]);

    // Social insurance table.
    $form['table_si'] = [
      '#type' => 'table',
      '#header' => [
        'month' => $this->t('Month'),
        'social_insurance' => $this->t('Social insurance'),
        'health_insurance' => $this->t('Health insurance'),
        'total_insurance' => $this->t('Total insurance'),
        'insurance_paid' => $this->t('Paid insurance'),
      ],
    ];

    foreach ($this->balanceEntities as $month => $month_data) {
      $form['table_si'][$month] = [
        'month' => ['#markup' => $month],
        'social_insurance' => ['#markup' => $month_data->social_insurance->value],
        'health_insurance' => ['#markup' => $month_data->health_insurance->value],
        'total_insurance' => [
          '#markup' => $month_data->social_insurance->value + $month_data->health_insurance->value,
        ],
        'insurance_paid' => [
          '#type' => 'textfield',
          '#default_value' => $month_data->insurance_paid->value,
        ],
      ];
    }

    $this->addTotalsRow($form['table_si'], [
      'social_insurance',
      'health_insurance',
      'total_insurance',
      'insurance_paid',
    ]);

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Recalculate and update'),
    ];
    $plugin_id = $this->config->get('tax_plugin_id');
    if ($this->readOnly || $plugin_id === NULL || $plugin_id === '') {
      $form['actions']['update']['#access'] = FALSE;
    }

    $form['#attached']['library'][] = 'polish_accounting/ui';

    return $form;
  }

  /**
   * Helper function that adds totals row for specified columns.
   */
  private function addTotalsRow(array &$table, array $total_columns) {
    $totals = [];
    foreach ($total_columns as $column) {
      $totals[$column] = 0;
    }

    foreach (Element::children($table) as $row_index) {
      $row = $table[$row_index];
      foreach ($total_columns as $column) {
        if (!\array_key_exists($column, $row)) {
          continue;
        }
        if (\array_key_exists('#markup', $row[$column])) {
          $totals[$column] += (float) $row[$column]['#markup'];
        }
        elseif (\array_key_exists('#default_value', $row[$column])) {
          $totals[$column] += (float) $row[$column]['#default_value'];
        }
      }
    }

    $totals_row = [
      '#attributes' => [
        'class' => ['accounting-totals'],
      ],
    ];
    foreach (\array_keys($table['#header']) as $column_key) {
      if (\array_key_exists($column_key, $totals)) {
        $totals_row[$column_key] = ['#markup' => $totals[$column_key]];
      }
      else {
        $totals_row[$column_key] = ['#markup' => ''];
      }
    }
    $table[] = $totals_row;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->readOnly) {
      return;
    }

    // @todo For larger data volumes this will need to be processed
    // in a batch operation.
    $year = $this->getYear($form_state);
    $plugin_settings = $this->config->get('tax_plugin_settings');
    $plugin_settings['year'] = $year;
    $tax_plugin = $this->taxMethodManager->createInstance($this->config->get('tax_plugin_id'), $plugin_settings);
    $form_state->cleanValues();
    $values = $form_state->getValues();
    $to_save = [];
    for ($i = 1; $i <= 12; $i++) {
      $to_save[$i] = [
        'income_tax_paid' => $values['table_tax'][$i]['income_tax_paid'],
        'vat_paid' => $values['table_tax'][$i]['vat_paid'],
        'notes' => $values['table_tax'][$i]['notes'],
        'insurance_paid' => $values['table_si'][$i]['insurance_paid'],
      ];
    }

    $results = $tax_plugin->calculate();

    foreach ($this->balanceEntities as $month => $balance_entity) {

      foreach ($results[$month] as $property => $value) {
        $balance_entity->set($property, $value);
      }
      foreach ($to_save[$month] as $property => $value) {
        $balance_entity->set($property, $value);
      }
      $balance_entity->save();
    }
  }

  /**
   * Helper method to get balance year.
   */
  private function getYear(FormStateInterface $form_state): int {
    $build_info = $form_state->getBuildInfo();
    $year = $build_info['args'][0];
    if ($year === NULL || $year === '') {
      $date = new DrupalDateTime();
      $year = $date->format('Y');
    }
    $year = (int) $year;
    return $year;
  }

}
