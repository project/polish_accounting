<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\polish_accounting\DocumentPdfGenerator;
use Drupal\polish_accounting\OccurrenceSynchronizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Global accounting settings form.
 */
final class AccountingDocumentForm extends ContentEntityForm {

  protected DocumentPdfGenerator $pdfGenerator;
  protected OccurrenceSynchronizer $occurrenceSynchronizer;

  /**
   * The obvious.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    DocumentPdfGenerator $pdf_generator,
    OccurrenceSynchronizer $occurrence_synchronizer
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->pdfGenerator = $pdf_generator;
    $this->occurrenceSynchronizer = $occurrence_synchronizer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('polish_accounting.pdf_generator'),
      $container->get('polish_accounting.occurrence_synchronizer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Figure out document_no if not provided.
    if ($this->entity->isNew()) {
      $form['document_no']['widget'][0]['value']['#default_value'] = $this->occurrenceSynchronizer->getDefaultDocumentNumber();

      // Set default date values as those cannot be set dynamically in
      // the widget settings.
      foreach ([
        'issue_date',
        'sale_date',
        'due_date',
      ] as $date_field) {
        $form[$date_field]['widget'][0]['value']['#default_value'] = $this->occurrenceSynchronizer->getFieldDate($date_field);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $seller_element = &$form['seller']['widget'][0]['target_id'];
    if ($seller_element['#default_value'] === NULL) {
      $default_seller_uuid = $this->config('polish_accounting.settings')->get('default_seller');
      if ($default_seller_uuid !== NULL && $default_seller_uuid !== '') {
        $contractor_entities = $this->entityTypeManager->getStorage('accounting_contractor')->loadByProperties(['uuid' => $default_seller_uuid]);
        if (\count($contractor_entities) !== 0) {
          $seller_element['#default_value'] = reset($contractor_entities);
        }
      }
    }

    $form['generate_pdf'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate PDF on submit'),
      '#weight' => 8,
    ];

    $template_options = [];
    foreach ($this->pdfGenerator->getTemplates() as $id => $template_data) {
      $template_options[$id] = $template_data['name'];
    }
    $form['pdf_template'] = [
      '#type' => 'select',
      '#title' => $this->t('PDF template'),
      '#options' => $template_options,
      '#weight' => 8,
      '#states' => [
        'visible' => [
          '[name="generate_pdf"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['file']['#states'] = [
      'visible' => [
        '[name="generate_pdf"]' => ['checked' => FALSE],
      ],
    ];

    $form['sync_occurrence'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Synchronize occurrence'),
      '#weight' => 10,
      '#default_value' => 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if ($form_state->getValue('generate_pdf') === 1) {
      $this->attachPdf($form_state);
    }

    $result = parent::save($form, $form_state);

    if ($form_state->getValue('sync_occurrence') === 1) {
      $occurrence = $this->occurrenceSynchronizer->occurrenceFromDocument($this->entity);
      $occurrence->save();
    }

    return $result;
  }

  /**
   * Generate and attach PDF to the document.
   */
  private function attachPdf(FormStateInterface $form_state): void {
    $template = $form_state->getValue('pdf_template');
    $document = $this->entity;

    try {
      $file = $this->pdfGenerator->generatePdf($document, $template);
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('Unable to generate document PDF: @error', [
        '@error' => $e->getMessage(),
      ]));
      $this->getLogger('polish_accounting')->error('%type: @message in %function (line %line of %file).', Error::decodeException($e));
      return;
    }

    // Delete previous document file reference if it existed.
    $file_field = $document->get('file');
    $file_field->delete();
    $field_item = $file_field->appendItem();

    $field_item->set('target_id', $file->id());
  }

}
