<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Global accounting settings form.
 */
final class AccountingContractorForm extends ContentEntityForm {

}
