<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Global accounting settings form.
 */
final class AccountingOccurrenceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Ensure files are saved in the correct location.
    $input = $form_state->getUserInput();
    $date = NestedArray::getValue($input, ['date', 0, 'value', 'date']);
    if ($date === NULL) {
      $date_field = $this->entity->get('date');
      if (!$date_field->isEmpty()) {
        $date = $date_field->value;
      }
    }
    if ($date !== NULL) {
      [$year, $month] = \explode('-', $date);
      $form['file']['widget'][0]['#upload_location'] = 'private://accounting/' . $year . '-' . $month;

    }

    return $form;
  }

}
