<?php

declare(strict_types=1);

namespace Drupal\polish_accounting;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\polish_accounting\Entity\AccountingDocument;
use Drupal\polish_accounting\Entity\AccountingOccurrence;

/**
 * Occurrence synchronizer class.
 */
class OccurrenceSynchronizer {

  use AccountingDatesTrait;

  /**
   * The entity type manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Accounting settings.
   */
  private ImmutableConfig $config;

  /**
   * The obvious.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('polish_accounting.settings');
  }

  /**
   * Creates an accouting occurrence from a document.
   */
  public function occurrenceFromDocument(AccountingDocument $document): AccountingOccurrence {
    if ($document->items->isEmpty()) {
      throw new InvalidArgumentException('Cannot create an occurrence from a document with no items.');
    }
    $values = [];
    if (!$document->description->isEmpty()) {
      $values['description'] = $document->description->value;
    }
    else {
      $parts = [];
      foreach ($document->items as $item) {
        $parts[] = $item->name;
      }
      $values['description'] = implode(', ', $parts);
    }

    $values['document_no'] = $document->document_no->value;
    $values['date'] = $document->sale_date->value;
    $values['document'] = $document;

    $values['contractor'] = $document->buyer->entity;
    $totals = $document->items->getTotals();
    $exchange_rate = $document->exchange_rate->value;
    $values['income_gs'] = round($totals['net'] * $exchange_rate, 2);
    $values['vat'] = round($totals['vat'] * $exchange_rate, 2);

    // Update or create.
    $occurrence_storage = $this->entityTypeManager->getStorage('accounting_occurrence');
    $occurrences = $occurrence_storage->loadByProperties([
      'document' => $document->id(),
    ]);
    if (\count($occurrences) !== 0) {
      $ocurrence = reset($occurrences);
      foreach ($values as $property => $value) {
        $ocurrence->set($property, $value);
      }
    }
    else {
      $ocurrence = $occurrence_storage->create($values);
    }

    return $ocurrence;
  }

  /**
   * Creates a document from occurrence and file.
   */
  public function documentFromOccurrence(AccountingOccurrence $occurrence, FileInterface $file): AccountingDocument {
    $seller_uuid = $this->config->get('default_seller');
    if ($seller_uuid === NULL || $seller_uuid === '') {
      throw new \InvalidArgumentException('Default seller is not set.');
    }
    $sellers = $this->entityTypeManager->getStorage('accounting_contractor')->loadByProperties(['uuid' => $seller_uuid]);
    if (\count($sellers) === 0) {
      throw new \InvalidArgumentException('Default seller doesn\'t exist.');
    }
    $seller = reset($sellers);

    $occurrence_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $occurrence->date->value);

    // Calculate due date.
    $payment_deadline_days = $this->config->get('payment_deadline_days');
    $occurrence_date->add(new \DateInterval('P' . $payment_deadline_days . 'D'));

    return $this->entityTypeManager->getStorage('accounting_document')->create([
      'document_no' => $occurrence->document_no->value,
      'issue_date' => $occurrence->date->value,
      'sale_date' => $occurrence->date->value,
      'due_date' => $occurrence_date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT),
      'seller' => $seller,
      'buyer' => $occurrence->contractor->entity,
      'description' => $occurrence->description->value,
      'file' => $file,
    ]);
  }

  /**
   * Get default document number.
   */
  public function getDefaultDocumentNumber() {
    $date = new DrupalDateTime();
    $document_numbers = [];
    $month_document_ids = $this->getMonthEntityIds($date, 'accounting_document', 'sale_date');
    if (\count($month_document_ids) !== 0) {
      $month_documents = $this->entityTypeManager->getStorage('accounting_document')->loadMultiple($month_document_ids);
      foreach ($month_documents as $document) {
        $document_no = $document->document_no->value;
        $document_numbers[$document_no] = TRUE;
      }
    }

    $counter = 1;
    do {
      // @todo Configurable pattern would be nice.
      $document_no = 'F-' . $date->format('m-Y') . '-' . $counter;
      $counter++;
    } while (\array_key_exists($document_no, $document_numbers));

    return $document_no;
  }

  /**
   * Get date string in date storage format.
   */
  public function getFieldDate(string $field) {
    if ($field === 'due_date') {
      $date = new DrupalDateTime('+' . $this->config->get('payment_deadline_days') . ' days');
    }
    else {
      $date = new DrupalDateTime();
    }
    return $date;
  }

}
