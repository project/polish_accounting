<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Accounting Contractor entity type.
 *
 * @ContentEntityType(
 *   id = "accounting_contractor",
 *   label = @Translation("Accounting contractor"),
 *   label_collection = @Translation("Contractors"),
 *   handlers = {
 *     "list_builder" = "Drupal\polish_accounting\ListBuilder\AccountingContractorListBuilder",
 *     "access" = "Drupal\polish_accounting\Access\AccountingContractorAccess",
 *     "form" = {
 *       "default" = "Drupal\polish_accounting\Form\AccountingContractorForm",
 *       "add" = "Drupal\polish_accounting\Form\AccountingContractorForm",
 *       "edit" = "Drupal\polish_accounting\Form\AccountingContractorForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\polish_accounting\Routing\AccountingContractorRouteProvider",
 *     },
 *   },
 *   base_table = "accounting_contractors",
 *   data_table = "accounting_contractors_data",
 *   admin_permission = "administer accounting",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/accounting/contractors/{accounting_contractor}",
 *     "collection" = "/accounting/contractors",
 *     "add-form" = "/accounting/contractors/add",
 *     "edit-form" = "/accounting/contractors/{accounting_contractor}/edit",
 *     "delete-form" = "/accounting/contractors/{accounting_contractor}/delete",
 *   },
 *   common_reference_target = TRUE,
 * )
 */
final class AccountingContractor extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Street address'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['postal_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Postal code'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('City'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['country'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Country'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['tin'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tax ID'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['account_no'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bank account number'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['bic'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bank Identifier Code (BIC)'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['logo'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Logo'))
      ->setSettings([
        'file_extensions' => 'jpg jpeg gif png svg',
        'file_directory' => 'accounting/contractors',
        'uri_scheme' => 'public',
        'alt_field' => 0,
        'alt_field_required' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'image',
        'settings' => [
          'image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
