<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Accounting Document entity type.
 *
 * @ContentEntityType(
 *   id = "accounting_month",
 *   label = @Translation("Accounting month"),
 *   label_collection = @Translation("Balance"),
 *   base_table = "accounting_balance",
 *   data_table = "accounting_balance_data",
 *   admin_permission = "administer accounting",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
final class AccountingMonth extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['year'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Year of the balance item'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('size', 'small');

    $fields['month'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Month of the balance item'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('size', 'tiny');

    $fields['income'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Income'))
      ->setSettings([
        'precision' => 15,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['cost'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Cost'))
      ->setSettings([
        'precision' => 15,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['income_tax'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Income tax'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['income_tax_paid'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Income tax paid'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['vat'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('VAT'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['vat_paid'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('VAT paid'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['social_insurance'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Social insurance'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['health_insurance'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Social insurance'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['insurance_paid'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Social insurance'))
      ->setSettings([
        'precision' => 10,
        'scale' => 2,
      ])
      ->setDefaultValue(0);

    $fields['notes'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Notes'));

    return $fields;
  }

}
