<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the Accounting Occurrence entity type.
 *
 * @ContentEntityType(
 *   id = "accounting_occurrence",
 *   label = @Translation("Accounting occurrence"),
 *   label_collection = @Translation("Occurrences"),
 *   handlers = {
 *     "list_builder" = "Drupal\polish_accounting\ListBuilder\AccountingOccurrenceListBuilder",
 *     "access" = "Drupal\polish_accounting\Access\AccountingOccurrenceAccess",
 *     "form" = {
 *       "default" = "Drupal\polish_accounting\Form\AccountingOccurrenceForm",
 *       "add" = "Drupal\polish_accounting\Form\AccountingOccurrenceForm",
 *       "edit" = "Drupal\polish_accounting\Form\AccountingOccurrenceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\polish_accounting\Routing\AccountingOccurrenceRouteProvider",
 *     },
 *   },
 *   base_table = "accounting_occurrences",
 *   data_table = "accounting_occurrences_data",
 *   admin_permission = "administer accounting",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "document_no",
 *   },
 *   links = {
 *     "canonical" = "/accounting/occurrences/{accounting_occurrence}",
 *     "collection" = "/accounting/occurrences",
 *     "add-form" = "/accounting/occurrences/add",
 *     "edit-form" = "/accounting/occurrences/{accounting_occurrence}/edit",
 *     "delete-form" = "/accounting/occurrences/{accounting_occurrence}/delete",
 *   },
 *   common_reference_target = FALSE,
 * )
 */
final class AccountingOccurrence extends ContentEntityBase {

  public const INCOME_FIELDS = [
    'income_gs' => 'Goods and services income',
    'income_other' => 'Other income',
  ];

  public const COST_FIELDS = [
    'cost_goods' => 'Trade goods purchase value',
    'cost_purchase' => 'Trade goods purchase additional costs',
    'cost_salaries' => 'Salaries costs',
    'cost_other' => 'Other costs',
  ];

  /**
   * Create an occurrence from document.
   */
  public static function createFromDocument(AccountingDocument $document) {
    return \Drupal::service('polish_accounting.occurrence_synchronizer')->occurrenceFromDocument($document);
  }

  /**
   * Get occurrence income.
   */
  public function getIncome(): float {
    return $this->getSum(\array_keys(self::INCOME_FIELDS));
  }

  /**
   * Get occurrence costs.
   */
  public function getCost(): float {
    return $this->getSum(\array_keys(self::COST_FIELDS));
  }

  /**
   * Get VAT.
   */
  public function getVat(): float {
    if ($this->vat->isEmpty()) {
      return 0;
    }
    return (float) $this->vat->value;
  }

  /**
   * Helper function that calculates sum of given fields.
   *
   * @param string[] $fields
   *   Array of field names.
   */
  private function getSum(array $fields): float {
    $sum = 0;
    foreach ($fields as $field) {
      $field = $this->get($field);
      if ($field->isEmpty()) {
        continue;
      }
      $sum += $field->value;
    }

    return $sum;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Short description'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['document_no'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Document number'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->addConstraint('UniqueField')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Occurrence date'))
      ->setDescription(t('Date of the ocurrence.'))
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['contractor'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Contractor'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'accounting_contractor')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'label',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
        'settings' => [],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $numeric_fields = self::INCOME_FIELDS + self::COST_FIELDS + [
      'vat' => t('VAT'),
    ];
    foreach ($numeric_fields as $numeric_field => $label) {
      $fields[$numeric_field] = BaseFieldDefinition::create('decimal')
        ->setLabel($label)
        ->setSettings([
          'precision' => 15,
          'scale' => 2,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'number_decimal',
          'settings' => [
            'thousand_separator' => ' ',
            'decimal_separator' => ',',
            'scale' => 2,
          ],
        ])
        ->setDisplayOptions('form', [
          'type' => 'number',
          'settings' => [],
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
    }

    $fields['remarks'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Remarks'))
      ->setSetting('case_sensitive', FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'settings' => [
          'rows' => '3',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['document'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Document'))
      ->setDescription(t('The document this occurrence references.'))
      ->setSetting('target_type', 'accounting_document')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'label',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
        'settings' => [],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Document file'))
      ->setSettings([
        'file_extensions' => 'pdf jpg jpeg',
        'file_directory' => 'accounting/[date:custom:Y]-[date:custom:m]',
        'uri_scheme' => 'private',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file_link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
