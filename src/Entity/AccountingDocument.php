<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the Accounting Document entity type.
 *
 * @ContentEntityType(
 *   id = "accounting_document",
 *   label = @Translation("Accounting document"),
 *   label_collection = @Translation("Documents"),
 *   handlers = {
 *     "list_builder" = "Drupal\polish_accounting\ListBuilder\AccountingDocumentListBuilder",
 *     "access" = "Drupal\polish_accounting\Access\AccountingDocumentAccess",
 *     "form" = {
 *       "default" = "Drupal\polish_accounting\Form\AccountingDocumentForm",
 *       "add" = "Drupal\polish_accounting\Form\AccountingDocumentForm",
 *       "edit" = "Drupal\polish_accounting\Form\AccountingDocumentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\polish_accounting\Routing\AccountingDocumentRouteProvider",
 *     },
 *   },
 *   base_table = "accounting_documents",
 *   data_table = "accounting_documents_data",
 *   admin_permission = "administer accounting",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "document_no",
 *   },
 *   links = {
 *     "canonical" = "/accounting/documents/{accounting_document}",
 *     "collection" = "/accounting/documents",
 *     "add-form" = "/accounting/documents/add",
 *     "edit-form" = "/accounting/documents/{accounting_document}/edit",
 *     "delete-form" = "/accounting/documents/{accounting_document}/delete",
 *   },
 *   common_reference_target = TRUE,
 * )
 */
final class AccountingDocument extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['document_no'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Document number'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->addConstraint('UniqueField')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['issue_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Issue date'))
      ->setDescription(t('Date when the document was issued.'))
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['sale_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Sale date'))
      ->setDescription(t('Date when the transaction occurred.'))
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['due_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Due date'))
      ->setDescription(t('Payment deadline.'))
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['seller'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Seller'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'accounting_contractor')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'label',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['buyer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Buyer'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'accounting_contractor')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'label',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['items'] = BaseFieldDefinition::create('accounting_document_item')
      ->setLabel(t('Document items'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'document_items_table',
      ])
      ->setDisplayOptions('form', [
        'type' => 'document_items_table',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['exchange_rate'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Exchange rate'))
      ->setDescription(t('Required if document currency is differrent than your accounting currency.'))
      ->setSettings([
        'precision' => 8,
        'scale' => 4,
      ])
      ->setDefaultValue(1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'settings' => [
          'thousand_separator' => ' ',
          'decimal_separator' => ',',
          'scale' => 4,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Short description'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Document file'))
      ->setSettings([
        'file_extensions' => 'pdf jpg jpeg',
        'file_directory' => 'accounting/[date:custom:Y]-[date:custom:m]',
        'uri_scheme' => 'private',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file_link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
