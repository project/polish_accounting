<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines custom methods for the document items field.
 */
final class AccountingDocumentItemList extends FieldItemList {

  /**
   * Get total values for all items.
   *
   * @return float[]
   *   Net and gross value of the document.
   */
  public function getTotals(): array {
    $result = [
      'net' => 0,
      'gross' => 0,
      'vat' => 0,
      'vat_summary' => [],
    ];
    foreach ($this as $item) {
      $net = $item->net_total;
      $vat = $item->vat_amount;
      $gross = $net + $vat;

      $result['net'] += $net;
      $result['vat'] += $vat;
      $result['gross'] += $gross;

      if (!\array_key_exists($item->vat, $result['vat_summary'])) {
        $result['vat_summary'][$item->vat] = [
          'net' => 0,
          'vat' => 0,
          'gross' => 0,
        ];
      }
      $result['vat_summary'][$item->vat]['net'] += $net;
      $result['vat_summary'][$item->vat]['vat'] += $vat;
      $result['vat_summary'][$item->vat]['gross'] += $gross;
    }

    return $result;
  }

}
