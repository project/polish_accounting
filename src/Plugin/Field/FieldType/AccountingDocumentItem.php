<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'serialized_data' field type.
 *
 * @FieldType(
 *   id = "accounting_document_item",
 *   label = @Translation("Accounting document item"),
 *   description = @Translation("This field stores accounting document item data."),
 *   default_widget = "document_items_table",
 *   default_formatter = "document_items_table",
 *   category = @Translation("Accounting"),
 *   list_class = "\Drupal\polish_accounting\Plugin\Field\FieldType\AccountingDocumentItemList",
 * )
 */
final class AccountingDocumentItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'unit' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'vat' => [
          'type' => 'numeric',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'precision' => 4,
          'scale' => 2,
        ],
        'quantity' => [
          'type' => 'float',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'net_price' => [
          'type' => 'numeric',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'precision' => 15,
          'scale' => 2,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Item name'))
      ->setRequired(TRUE);
    $properties['unit'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Item unit'))
      ->setRequired(TRUE);
    $properties['vat'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('VAT'))
      ->setRequired(TRUE);
    $properties['quantity'] = DataDefinition::create('float')
      ->setLabel(new TranslatableMarkup('Quantity'))
      ->setRequired(TRUE);
    $properties['net_price'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Net price'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'name' => 'Item 1',
      'unit' => 'piece',
      'vat' => 22.5,
      'quantity' => 5.4,
      'net_price' => 123.45,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    if ($name === 'net_total') {
      return $this->net_price * $this->quantity;
    }
    elseif ($name === 'vat_amount') {
      return \round($this->net_total * $this->vat / 100, 2);
    }
    elseif ($name === 'gross_total') {
      return $this->net_total + $this->vat_amount;
    }

    return parent::__get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    if ($name === 'net_total' || $name === 'vat_amount' || $name === 'gross_total') {
      return $this->__get($name);
    }

    return parent::get($name);
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): string {
    return 'name';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    if ($this->name === NULL || $this->name === '') {
      return TRUE;
    }
    if ($this->quantity === NULL || $this->quantity === 0) {
      return TRUE;
    }
    if ($this->net_price === NULL || $this->net_price === 0) {
      return TRUE;
    }

    return FALSE;
  }

}
