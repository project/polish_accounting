<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the document items amount formatter.
 *
 * @FieldFormatter (
 *   id = "document_items_amount",
 *   label = @Translation("Amount"),
 *   field_types = {
 *     "accounting_document_item"
 *   }
 * )
 */
final class DocumentItemsAmount extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL): array {
    return [
      0 => [
        '#markup' => $items->getTotals()['gross'],
      ],
    ];
  }

}
