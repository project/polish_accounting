<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the document items table formatter.
 *
 * @FieldFormatter (
 *   id = "document_items_table",
 *   label = @Translation("Items table"),
 *   field_types = {
 *     "accounting_document_item"
 *   }
 * )
 */
final class DocumentItemsTable extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['currency_code' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['currency_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#options' => [
        '' => $this->t('None'),
        'USD' => 'USD',
        'PLN' => 'PLN',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $currency = $this->getSetting('currency_code');
    return [
      $this->t('Currency: @currency', [
        '@currency' => $currency === '' ? 'none' : $currency,
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL): array {
    $currency = $this->getSetting('currency_code');
    $currency_suffix = $currency === '' ? '' : " [$currency]";
    $renderable = [
      '#theme' => 'table',
      '#empty' => $this->t('There are no items'),
      '#header' => [
        'i' => $this->t('No.'),
        'name' => $this->t('Item'),
        'net_price' => $this->t('Net price') . $currency_suffix,
        'unit' => $this->t('Unit'),
        'quantity' => $this->t('Quantity'),
        'net_total' => $this->t('Gross price') . $currency_suffix,
        'vat' => $this->t('VAT [%]'),
        'vat_amount' => $this->t('VAT amount') . $currency_suffix,
        'gross_total' => $this->t('Gross amount') . $currency_suffix,
      ],
      '#rows' => [],
    ];
    foreach ($items as $delta => $item) {
      $renderable['#rows'][] = [
        'i' => ['data' => $delta + 1],
        'name' => ['data' => $item->name],
        'net_price' => ['data' => $item->net_price],
        'unit' => ['data' => $item->unit],
        'quantity' => ['data' => $item->quantity],
        'net_total' => ['data' => $item->net_total],
        'vat' => ['data' => $item->vat],
        'vat_amount' => ['data' => $item->vat_amount],
        'gross_total' => ['data' => $item->gross_total],
      ];
    }

    $totals_data = $items->getTotals();
    $totals_row = [
      'label' => [
        'data' => $this->t('Totals'),
        'colspan' => 5,
      ],
      'net_total' => ['data' => $totals_data['net']],
      'vat' => ['data' => ''],
      'vat_amount' => ['data' => $totals_data['vat']],
      'gross_total' => ['data' => $totals_data['gross']],
    ];
    $renderable['#rows'][] = $totals_row;
    return [
      0 => $renderable,
    ];
  }

}
