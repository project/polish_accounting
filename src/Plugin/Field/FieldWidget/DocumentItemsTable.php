<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'document_items_table' widget.
 *
 * @FieldWidget (
 *   id = "document_items_table",
 *   label = @Translation("Document items table"),
 *   field_types = {
 *     "accounting_document_item"
 *   },
 *   multiple_values = TRUE,
 * )
 */
class DocumentItemsTable extends WidgetBase {

  private const ID_SUFFIX = '-document-items-table';

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = [];
    $element['#type'] = 'details';
    $element['#title'] = $this->t('Document items');
    $element['#open'] = TRUE;

    // Try to get items from form state values first.
    $value_parents = $element['#field_parents'];
    $value_parents[] = $items->getName();
    $value_parents[] = 'table';
    $values = $form_state->getValue($value_parents);
    if ($values === NULL) {
      $values = [];
      foreach ($items as $item) {
        $values[] = $item->getValue();
      }
      if (\count($values) === 0) {
        $values[] = $this->getEmptyRow();
      }
    }

    $ajax = [
      'callback' => [$this, 'elementAjax'],
      'wrapper' => $items->getName() . self::ID_SUFFIX,
    ];

    $element['table'] = [
      '#type' => 'table',
      '#empty' => $this->t('There are no items'),
      '#header' => [],
      '#attributes' => ['id' => $ajax['wrapper']],
    ];

    $input_types = [
      'name' => ['#type' => 'textfield'],
      'unit' => ['#type' => 'textfield'],
      'vat' => [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 100,
        '#step' => 0.1,
      ],
      'quantity' => [
        '#type' => 'number',
        '#min' => 0,
        '#step' => 0.01,
      ],
      'net_price' => [
        '#type' => 'number',
        '#min' => 0,
        '#step' => 0.01,
      ],
    ];

    foreach ($this->fieldDefinition->getPropertyDefinitions() as $key => $definition) {
      $element['table']['#header'][$key] = $definition->getLabel();
    }
    $element['table']['#header']['_delete'] = '';

    foreach ($values as $delta => $row) {
      unset($row['_delete']);

      $element['table'][$delta] = [];
      foreach ($row as $cell_key => $default_value) {
        $element['table'][$delta][$cell_key] = $input_types[$cell_key];
        $element['table'][$delta][$cell_key]['#default_value'] = $default_value;
      }
      $element['table'][$delta]['_delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove item'),
        '#ajax' => $ajax,
        '#name' => 'delete_' . $delta,
        '#submit' => [[$this, 'ajaxSubmit']],
        '#limit_validation_errors' => [$value_parents],
      ];
    }
    $element['_add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add item'),
      '#ajax' => $ajax,
      '#submit' => [[$this, 'ajaxSubmit']],
      '#limit_validation_errors' => [$value_parents],
    ];
    return $element;
  }

  /**
   * Ajax callback for adding / removing rows.
   */
  public function elementAjax(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    $this->processParents($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Submit handler that takes care of row addition / deletion.
   */
  public function ajaxSubmit(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $parents = $form_state->getTriggeringElement()['#parents'];
    $delta = NULL;
    $op = $this->processParents($parents, $delta);
    $values = $form_state->getValue($parents);
    if ($op === '_add') {
      $values[] = $this->getEmptyRow();
    }
    elseif ($op === '_delete') {
      unset($values[$delta]);
    }
    $form_state->setValue($parents, $values);
  }

  /**
   * Helper function.
   *
   * Gets the performed operation and processes value parents.
   */
  private function processParents(array &$parents, ?int &$delta = NULL): string {
    $op = array_pop($parents);
    if ($op === '_delete') {
      $delta = array_pop($parents);
    }
    elseif ($op === '_add') {
      $parents[] = 'table';
    }
    return $op;
  }

  /**
   * Helper function that returns an empty value row.
   */
  private function getEmptyRow(): array {
    return [
      'name' => '',
      'unit' => '',
      'vat' => 0,
      'quantity' => 1,
      // @todo Dependency injection.
      'net_price' => \Drupal::config('polish_accounting.settings')->get('default_rate'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $output = [];
    if (\array_key_exists('table', $values)) {
      foreach ($values['table'] as $delta => $item) {
        unset($item['_delete']);
        $output[$delta] = $item;
      }
    }

    return $output;
  }

}
