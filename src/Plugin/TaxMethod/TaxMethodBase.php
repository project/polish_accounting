<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\TaxMethod;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\polish_accounting\TaxMethodInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for tax methods.
 */
abstract class TaxMethodBase extends PluginBase implements TaxMethodInterface, ContainerFactoryPluginInterface {

  /**
   * Social insurance data.
   */
  protected const SI_DATA = [
    'health' => 9,
    'retirement' => 19.52,
    'rent' => 8,
    'accident' => 1.67,
    'work' => 2.45,
    'sickness' => 2.45,
  ];

  /**
   * Income / cost / tax values for all months.
   *
   * @var float[][]
   */
  private ?array $monthValues = NULL;

  /**
   * The year the calculations are for.
   */
  private int $year;

  /**
   * The obvious.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    foreach ($this->defaultConfiguration() as $key => $value) {
      if (!\array_key_exists($key, $this->configuration)) {
        $this->configuration[$key] = $value;
      }
    }

    $this->entityTypeManager = $entity_type_manager;
    $this->year = $configuration['year'] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get month values.
   */
  public function getMonthValues(): array {
    if ($this->monthValues !== NULL) {
      return $this->monthValues;
    }

    $occurrence_storage = $this->entityTypeManager->getStorage('accounting_occurrence');
    $query = $occurrence_storage->getQuery();
    $results = $query
      ->accessCheck(FALSE)
      ->condition('date', $this->year . '-01-01', '>=')
      ->condition('date', $this->year . '-12-31', '<=')
      ->execute();
    $occurrences = $occurrence_storage->loadMultiple($results);

    $this->monthValues = [];
    for ($i = 1; $i <= 12; $i++) {
      $this->monthValues[$i] = [
        'income' => 0,
        'cost' => 0,
        'vat' => 0,
      ];
    }

    foreach ($occurrences as $occurrence) {
      $date = $occurrence->date->value;
      [, $month] = \explode('-', $date);
      $month = (int) $month;
      $income = $occurrence->getIncome();
      $cost = $occurrence->getCost();
      $this->monthValues[$month]['income'] += $income;
      $this->monthValues[$month]['cost'] += $cost;
      if ($income > $cost) {
        $this->monthValues[$month]['vat'] += $occurrence->getVat();
      }
      else {
        $this->monthValues[$month]['vat'] -= $occurrence->getVat();
      }
    }

    return $this->monthValues;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $element, FormStateInterface $form_state): array {
    $element['sickness_rate_included'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is sickness rate included?'),
      '#default_value' => $this->getConfiguration('sickness_rate_included'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['sickness_rate_included' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(string $key) {
    if (\array_key_exists($key, $this->configuration)) {
      return $this->configuration[$key];
    }

    $defaults = $this->defaultConfiguration();
    if (\array_key_exists($key, $defaults)) {
      return $defaults[$key];
    }

    return NULL;
  }

}
