<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Plugin\TaxMethod;

use Drupal\Core\Form\FormStateInterface;

/**
 * Implementation of the "Rychalt" tax method.
 *
 * @TaxMethod(
 *   id = "rychalt",
 *   title = @Translation("Rychalt"),
 * )
 */
final class Rychalt extends TaxMethodBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'si_health_base' => 6965.94,
      'si_social_base' => 4161,
      'income_group' => 2,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $element, FormStateInterface $form_state): array {
    $element = parent::buildConfigurationForm($element, $form_state);

    $element['si_health_base'] = [
      '#title' => $this->t('Health insurance base amount'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#default_value' => $this->getConfiguration('si_health_base'),
    ];

    $element['si_social_base'] = [
      '#title' => $this->t('Social insurance base amount'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 0.01,
      '#default_value' => $this->getConfiguration('si_social_base'),
    ];

    $element['income_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Income group'),
      '#options' => [
        1 => $this->t('< 60 000 PLN'),
        2 => $this->t('>= 60 000 PLN, <= 300 000 PLN'),
        3 => $this->t('> 300 000 PLN'),
      ],
      '#default_value' => $this->getConfiguration('income_group'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function calculate(): array {
    $results = [];

    $month_values = $this->getMonthValues();

    $health_base = $this->getConfiguration('si_health_base');
    $income_group = $this->getConfiguration('income_group');

    $social_insurance_components = static::SI_DATA;
    if (!$this->getConfiguration('sickness_rate_included')) {
      unset($social_insurance_components['sickness']);
    }
    unset($social_insurance_components['health']);

    if ($income_group === 1) {
      $health_insurance_base = $health_base * 0.6;
    }
    elseif ($income_group === 2) {
      $health_insurance_base = $health_base * 1;
    }
    elseif ($income_group === 3) {
      $health_insurance_base = $health_base * 1.8;
    }
    $health_insurance_base = round($health_insurance_base, 2);
    $social_insurance_base = $this->getConfiguration('si_social_base');

    $results = [];
    $increasing[0] = [
      'tax_base' => 0,
      'income_tax' => 0,
      'health_insurance' => 0,
    ];
    for ($month = 1; $month <= 12; $month++) {
      $results[$month]['income'] = $month_values[$month]['income'];
      $results[$month]['cost'] = $month_values[$month]['cost'];
      $results[$month]['vat'] = $month_values[$month]['vat'];
      $results[$month]['social_insurance'] = 0;
      foreach ($social_insurance_components as $rate) {
        $results[$month]['social_insurance'] += $social_insurance_base * $rate / 100;
      }
      $results[$month]['health_insurance'] = $health_insurance_base * static::SI_DATA['health'] / 100;

      // @todo Calculate tax if ever needed.
      $results[$month]['income_tax'] = 0;

      // Round results to 2 decimals except tax.
      foreach ($results[$month] as $key => &$value) {
        if ($key === 'income_tax') {
          $value = round($value);
        }
        else {
          $value = round($value, 2);
        }
      }
    }

    return $results;
  }

}
