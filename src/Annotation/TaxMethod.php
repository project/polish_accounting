<?php

namespace Drupal\polish_accounting\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for tax method plugins.
 *
 * @see \Drupal\polish_accounting\Plugin\TaxMethod\TaxMethodBase
 *
 * @ingroup accounting_plugins
 *
 * @Annotation
 */
class TaxMethod extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin title used in the module UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title = '';

}
