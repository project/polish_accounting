<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access handler for contractor entities.
 */
abstract class AccountingEntityAccessBase extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation === 'view') {
      return AccessResult::allowedIfHasPermissions($account, [
        'use accounting',
        'view accounting',
        'administer accounting',
      ], 'OR');
    }
    else {
      return AccessResult::allowedIfHasPermissions($account, [
        'use accounting',
        'administer accounting',
      ], 'OR');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess($entity_bundle = NULL, AccountInterface $account = NULL, array $context = [], $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermissions($account, [
      'use accounting',
      'administer accounting',
    ], 'OR');
  }

}
