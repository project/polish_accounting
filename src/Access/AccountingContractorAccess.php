<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Access;

/**
 * Access handler for contractor entities.
 */
final class AccountingContractorAccess extends AccountingEntityAccessBase {

}
