<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Access;

/**
 * Access handler for document entities.
 */
final class AccountingDocumentAccess extends AccountingEntityAccessBase {

}
