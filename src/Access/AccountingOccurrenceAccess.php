<?php

declare(strict_types=1);

namespace Drupal\polish_accounting\Access;

/**
 * Access handler for occurrence entities.
 */
final class AccountingOccurrenceAccess extends AccountingEntityAccessBase {

}
