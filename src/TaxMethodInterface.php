<?php

declare(strict_types=1);

namespace Drupal\polish_accounting;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for tax method plugins.
 */
interface TaxMethodInterface {

  /**
   * Calculate income tax, revenue, vat and social insurance for every month.
   */
  public function calculate(): array;

  /**
   * Get plugin settings form.
   */
  public function buildConfigurationForm(array $element, FormStateInterface $form_state): array;

  /**
   * Gets default configuration for this plugin.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array;

  /**
   * Gets this plugin's configuration item.
   *
   * @return mixed
   *   The value of the requested configuration item.
   */
  public function getConfiguration(string $key);

}
