<?php

declare(strict_types=1);

namespace Drupal\polish_accounting;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Common date methods.
 */
trait AccountingDatesTrait {

  /**
   * There clearly is a better way to implement this.
   */
  protected function getMonthEntityIds(DrupalDateTime $date, string $entity_type_id, string $date_field, bool $access_check = FALSE, ?int $pager_limit = NULL): array {
    if (property_exists($this, 'storage')) {
      $storage = $this->storage;
    }
    else {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
    }
    return $this->doGetMonthEntityIds($date, $storage, $date_field, $access_check, $pager_limit);
  }

  /**
   * Helper method that returns entities from given date's month.
   *
   * @return string[]
   *   Entity IDs.
   */
  protected function doGetMonthEntityIds(DrupalDateTime $date, EntityStorageInterface $entity_storage, string $date_field, bool $access_check, ?int $pager_limit): array {
    $start = [
      'y' => $date->format('Y'),
      'm' => $date->format('m'),
      'd' => 1,
    ];
    $end = $start;
    $end['d'] = $date->format('t');

    // This method will modify date passed by reference so we need to clone
    // it before it happens.
    $temporary_date = clone $date;

    $temporary_date->setDate($start['y'], $start['m'], $start['d']);
    $query = $entity_storage->getQuery();
    $query->accessCheck($access_check);
    $query->condition($date_field, $temporary_date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT), '>=');
    $temporary_date->setDate($end['y'], $end['m'], $end['d']);
    $query->condition($date_field, $temporary_date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT), '<=');

    if ($pager_limit !== NULL && $pager_limit !== 0) {
      $query->pager($pager_limit);
    }

    $results = $query->execute();

    return $results;
  }

  /**
   * As the above but from string.
   */
  protected function getMonthEntityIdsFromStr(string $date_str, string $date_field, ?int $pager_limit): array {
    if ($date_str === '') {
      $date = new DrupalDateTime();
    }
    else {
      $date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $date_str);
    }
    // This is used in list builders so we are adding access check and pager.
    // Also storage is available.
    return $this->getMonthEntityIds($date, '', $date_field, TRUE, $pager_limit);
  }

  /**
   * Get current, last and next year and month for filters.
   */
  protected function getFilterDates(string $initial_value = ''): array {
    $output = [];

    if ($initial_value === '') {
      $date = new DrupalDateTime();
      [$current_year, $current_month] = \explode('-', $date->format('Y-m'));
    }
    else {
      [$current_year, $current_month] = \explode('-', $initial_value);
    }

    for ($i = -1; $i < 2; $i += 2) {
      $month = $current_month + $i;
      $year = $current_year;
      if ($month === 13) {
        $month = 1;
        $year++;
      }
      if ($month === 0) {
        $month = 12;
        $year--;
      }

      if ($month < 10) {
        $month = '0' . $month;
      }

      $output[$i] = $year . '-' . $month;
    }

    return $output;
  }

}
